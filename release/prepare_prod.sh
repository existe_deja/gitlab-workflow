#!/bin/sh
export APP_VERSION=$1

# Fancy terminal
GREEN='\033[0;32m'
NC='\033[0m' # No Color

# build every stage of the multistage build
printf "${GREEN}Build all docker stages...${NC}\n"
printf "${GREEN}Build base stage...${NC}\n"
docker build --build-arg BUILDKIT_INLINE_CACHE=1 \
  --file ./docker/Dockerfile \
  --cache-from $CI_REGISTRY_IMAGE/base:latest \
  --tag $CI_REGISTRY_IMAGE/base:latest \
  --target base .

printf "${GREEN}Build dependencies stage...${NC}\n"
docker build --build-arg BUILDKIT_INLINE_CACHE=1 \
  --file ./docker/Dockerfile \
  --cache-from $CI_REGISTRY_IMAGE/base:latest \
  --cache-from $CI_REGISTRY_IMAGE/dependencies:latest \
  --tag $CI_REGISTRY_IMAGE/dependencies:latest \
  --target dependencies .

printf "${GREEN}Build builder stage...${NC}\n"
docker build --build-arg BUILDKIT_INLINE_CACHE=1 \
  --file ./docker/Dockerfile \
  --cache-from $CI_REGISTRY_IMAGE/base:latest \
  --cache-from $CI_REGISTRY_IMAGE/dependencies:latest \
  --cache-from $CI_REGISTRY_IMAGE/builder:latest \
  --tag $CI_REGISTRY_IMAGE/builder:latest \
  --target builder .

printf "${GREEN}Build prod stage...${NC}\n"
# cache from $CI_REGISTRY_IMAGE:latest will always be invalidated
docker build --build-arg BUILDKIT_INLINE_CACHE=1 \
  --file ./docker/Dockerfile \
  --cache-from $CI_REGISTRY_IMAGE/base:latest \
  --cache-from $CI_REGISTRY_IMAGE/dependencies:latest \
  --cache-from $CI_REGISTRY_IMAGE/builder:latest \
  --tag $CI_REGISTRY_IMAGE:latest \
  --tag $CI_REGISTRY_IMAGE:$APP_VERSION \
  --target prod .

# push images to the container registry
printf "${GREEN}Updating container registries...${NC}\n"
docker push $CI_REGISTRY_IMAGE/base:latest
docker push $CI_REGISTRY_IMAGE/dependencies:latest
docker push $CI_REGISTRY_IMAGE/builder:latest
docker push $CI_REGISTRY_IMAGE:latest
